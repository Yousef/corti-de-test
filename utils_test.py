from unittest import TestCase, main
from utils import read_audio, write_audio, transform_audio
from pydub import AudioSegment
import os
import shutil


class UtilsTestCase(TestCase):
    def setUp(self):
        # setup temporay directory to write to
        self.test_dir = "./tmp"
        if not os.path.exists(self.test_dir):
            os.makedirs(self.test_dir)

    def tearDown(self):
        # delete temporary directory and files
        shutil.rmtree(self.test_dir)

    def test_read_audio(self):
        # should read .wav and .mp3 files and return AudioSegment
        audio_wav = read_audio("./data/1.wav", "wav")
        audio_mp3 = read_audio("./data/2.mp3", "mp3")
        self.assertIsInstance(audio_wav, AudioSegment)
        self.assertIsInstance(audio_mp3, AudioSegment)

        # should return appropriate exception
        with self.assertRaises(Exception) as context:
            read_audio("./data/6.wav", "wav")

        self.assertTrue("./data/6.wav is invalid", context.exception)

    def test_transform_audio(self):
        audio = AudioSegment.silent()
        _audio = transform_audio(audio)

        self.assertEqual(_audio.channels, 1)
        self.assertEqual(_audio.sample_width, 2)
        self.assertEqual(_audio.frame_rate, 8000)

    def test_write_audio(self):
        fpath = self.test_dir + "/test_file.wav"
        audio = AudioSegment.silent()

        write_audio(fpath, audio)
        self.assertTrue(os.path.exists(fpath))


if __name__ == "__main__":
    main()
