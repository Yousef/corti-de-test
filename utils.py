from pydub import AudioSegment
from pydub.exceptions import CouldntDecodeError


def read_audio(fpath, ftype):
    try:
        audio = AudioSegment.from_file(fpath, ftype)
        return audio
    except CouldntDecodeError:
        raise Exception("{} is invalid".format(fpath))


def transform_audio(audio):
    # mono channel, sample width of 2 bytes (16 bit), frame-rate 8000Hz
    _audio = audio.set_channels(1)
    _audio = audio.set_sample_width(2)
    _audio = audio.set_frame_rate(8000)

    return _audio


def write_audio(fpath, audio):
    # pydub doesn't close filebuffer appropriately
    audio.export(fpath, format="wav")
