from queue import Queue
import argparse
from utils import read_audio, write_audio
from glob import glob
import os
import sys


if __name__ == "__main__":
    q = Queue()
    # get input and output directories from cmd arguments
    parser = argparse.ArgumentParser(description="Process audio files.")
    parser.add_argument("input_folder", type=str,
                        help="path to folder with audio files to process")
    parser.add_argument("output_folder", type=str,
                        help="path to destination folder")
    args = parser.parse_args()

    # make sure folders exists
    if not os.path.isdir(args.input_folder):
        print("input folder {} doesn't exist.".format(args.input_folder))
        sys.exit()

    if not os.path.isdir(args.output_folder):
        # make output folder optional, use default "./dest"
        print("output folder doesn't exist, creating {}".format(args.output_folder))
        os.makedirs(args.output_folder)

        # get supported audio files from input directory
    file_extensions = ("wav", "mp3")
    audio_files = [glob("{}*.{}".format(args.input_folder, ext))
                   for ext in file_extensions]

    # read audio segment from file and put in queue
    def queue_audio(fpath, idx):
        try:
            ftype = file_extensions[idx]
            audio = read_audio(fpath, ftype)
            fname = fpath.replace(args.input_folder, "")
            q.put((audio, fname))
        except Exception as e:
            print(e)

    [queue_audio(fpath, idx)
     for idx, sublist in enumerate(audio_files) for fpath in sublist]

    # read audio segment from queue and write to file
    while not q.empty():
        audio, fname = q.get()
        fpath = args.output_folder+fname

        write_audio(fpath, audio)
