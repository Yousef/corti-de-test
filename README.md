# Project

Command line tool to process audio recordings and save them to disk with specified format.

## Getting started

### Installation

1.  Make sure you have Python >= 3.5 installed:

        1.  pip install requirements.txt

1.  using virtual environment:

        1. virtual venv
        2. source venv/bin/activate
        3. pip install -r requirements.txt

### Usage

Run application from commandline:

        python index.py [input_folder] [destination_folder]

Example:

        python index.py ./data/ ./dest/

## Design choices and alternative approaches

I choose a more functional approach, breaking the tasks down into modular functional units. Ideally self-contained, modular functions are easy to test, refactor and compose into more complex operations.

Alternatively, one could have chosen a more object-oriented approach.

## Scaling strategy

- Use asynchronous task queue such as Celery, to execute tasks concurrently.
- Store audio recordings in S3 buckets.
- Use message queue (RabbitMQ) and/or lambda function to write processed audio to S3.
